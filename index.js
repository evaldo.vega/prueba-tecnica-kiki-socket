var express = require("express");
const path = require("path");
var app = express();
const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server,{
  cors:{
    origin:'*'
  }
});

//app.use(express.static("public"));
app.get("/", function (req, res) {
  res.send("Welcome Kiki stats");
});

io.on("connection", (socket) => {
  console.log("a user connected");

  socket.on("stats", (payload) => {
    socket.broadcast.emit('stats',payload)
  });

  socket.emit("welcome", "Welcome Kiki stats");
});

server.listen(3001);